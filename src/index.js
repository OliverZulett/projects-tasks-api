const express = require('express');
const connectDB = require('./config/db');

const app = express();
const PORT = process.env.PORT || 4000;

const cors = require('cors');

connectDB();

app.use(cors());

app.use(express.json({ extended: true }));

app.use('/v1/healthCheck', require('./routes/healthCheck'));
app.use('/v1/users', require('./routes/users'));
app.use('/v1/auth', require('./routes/auth'));
app.use('/v1/projects', require('./routes/project'));
app.use('/v1/projects', require('./routes/task'));

app.listen(PORT, () => {
  console.log(`Server is listening in port: ${PORT}`);
});
