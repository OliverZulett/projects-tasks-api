const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const userController = require('../controllers/userController');

router.post(
  '/',
  [
    check('name', 'Name es required').not().isEmpty(),
    check('email', 'Invalid Email').isEmail(),
    check('password', 'Password must have at least 6 characters').isLength({
      min: 6,
    }),
  ],
  userController.createUser
);

module.exports = router;
