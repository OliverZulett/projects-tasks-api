const healthCheckController = require('../controllers/healthCheckController');
const router = require('./users');

router.get('/', healthCheckController.checkHealth);

module.exports = router;