const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const authController = require('../controllers/authController');
const auth = require('../middleware/auth');

router.post('/', [authController.authUser]);
router.get('/', [auth, authController.getUserAuthenticated]);

module.exports = router;
