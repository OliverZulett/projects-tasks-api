const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const projectController = require('../controllers/projectController');
const taskController = require('../controllers/taskController');
const auth = require('../middleware/auth');

router.post(
  '/',
  auth,
  [check('name', 'Project name cannot be empty').not().isEmpty()],
  projectController.createProject
);
router.get('/', auth, projectController.getProjects);
router.put(
  '/:id',
  auth,
  [check('name', 'Project name cannot be empty').not().isEmpty()],
  projectController.updateProject
);
router.delete('/:id', auth, projectController.deleteProject);

module.exports = router;
