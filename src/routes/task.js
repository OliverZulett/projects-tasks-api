const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const taskController = require('../controllers/taskController');
const auth = require('../middleware/auth');
const validate = require('../middleware/validate');
const authProject = require('../middleware/authProject');

router.post(
  '/:projectId/tasks',
  auth,
  validate,
  authProject,
  [check('name', 'Task name cannot be empty').not().isEmpty()],
  taskController.createTask
);
router.get('/:projectId/tasks', auth, authProject, taskController.getTasks);
router.put(
  '/:projectId/tasks/:taskId',
  auth,
  validate,
  authProject,
  [check('name', 'Task name cannot be empty').not().isEmpty()],
  taskController.updateTask
);
router.delete(
  '/:projectId/tasks/:taskId',
  auth,
  authProject,
  taskController.deleteTask
);

module.exports = router;
