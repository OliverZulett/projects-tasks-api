const Project = require('../models/Project');
const { validationResult } = require('express-validator');

exports.createProject = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const project = new Project(req.body);
    project.creator = req.user.id;
    project.save();
    res.json(project);
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'project creation has failed' });
  }
};

exports.getProjects = async (req, res) => {
  try {
    const projects = await Project.find({ creator: req.user.id }).sort({
      created_at: -1,
    });
    res.json({ total: projects.length, ...{ projects } });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'error finding projects' });
  }
};

exports.updateProject = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  const { name } = req.body;
  const projectUpdated = {};
  if (name) {
    projectUpdated.name = name;
  }
  try {
    let project = await Project.findById(req.params.id);
    if (!project) {
      return res.status(404).json({ message: 'project not found' });
    }
    if (project.creator.toString() !== req.user.id) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    project = await Project.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: projectUpdated },
      { new: true }
    );
    res.json({ project });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'error updating project' });
  }
};

exports.deleteProject = async (req, res) => {
  try {
    let project = await Project.findById(req.params.id);
    if (!project) {
      return res.status(404).json({ message: 'project not found' });
    }
    if (project.creator.toString() !== req.user.id) {
      return res.status(401).json({ message: 'Unauthorized' });
    }
    project = await Project.findOneAndRemove({ _id: req.params.id });
    res.status(200).send({ message: 'project deleted successfully' });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'error deleting project' });
  }
};
