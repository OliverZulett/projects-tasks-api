const mongoose = require('mongoose');

exports.checkHealth = async (req, res) => {
  res
    .status(200)
    .send({
      message: `I'm Alive`,
      db_status: mongoose.STATES[mongoose.connection.readyState],
    });
};
