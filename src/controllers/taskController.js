const Task = require('../models/Task');

exports.createTask = async (req, res) => {
  try {
    const task = new Task(req.body);
    task.project = req.params.projectId;
    await task.save();
    res.status(201).send(task);
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'task creation has failed' });
  }
};

exports.getTasks = async (req, res) => {
  try {
    const tasks = await Task.find({ project: req.project }).sort({
      created_at: -1,
    });
    res.json({ total: tasks.length, ...{ tasks } });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'error getting tasks' });
  }
};

exports.updateTask = async (req, res) => {
  const { name, state } = req.body;
  const taskUpdated = {};
  taskUpdated.name = name;
  taskUpdated.state = state;
  try {
    let task = await Task.findById(req.params.taskId);
    if (!task) {
      return res.status(404).json({ message: 'task not found' });
    }
    task = await Task.findByIdAndUpdate(
      { _id: req.params.taskId },
      { $set: taskUpdated },
      { new: true }
    );
    res.json(task);
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'task updating has failed' });
  }
};

exports.deleteTask = async (req, res) => {
  try {
    let task = await Task.findById(req.params.taskId);
    if (!task) {
      return res.status(404).json({ message: 'task not found' });
    }
    task = await Task.findOneAndRemove({ _id: req.params.taskId });
    res.status(200).send({ message: 'task deleted successfully' });
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: 'error deleting task' });
  }
};
