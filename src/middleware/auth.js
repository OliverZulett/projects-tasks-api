const jwt = require('jsonwebtoken');
module.exports = function (req, res, next) {
  const bearerHeader = req.headers['authorization'];
  if (typeof bearerHeader !== 'undefined') {
    const bearerToken = bearerHeader.split(' ')[1];
    try {
      const encrypted = jwt.verify(bearerToken, process.env.SECRET);
      req.user = encrypted.user;
      next();
    } catch (error) {
      return res.status(403).json({ message: 'unauthorized, invalid token' });
    }
  } else {
    return res.status(403).json({ message: 'unauthorized, no token provided' });
  }
};
