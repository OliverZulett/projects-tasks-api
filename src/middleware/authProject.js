const Project = require('../models/Project');

module.exports = async function (req, res, next) {
  const project = await Project.findById(req.params.projectId);
  if (!project) {
    return res.status(404).json({ message: 'project not found' });
  }
  if (project.creator.toString() !== req.user.id) {
    return res.status(401).json({ message: 'Unauthorized' });
  }
  req.project = project;
  next();
};
