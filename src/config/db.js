const mongoose = require('mongoose');
require('dotenv').config({ path: '.env' });

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    console.log({db_status: mongoose.STATES[mongoose.connection.readyState]});
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

module.exports = connectDB;
