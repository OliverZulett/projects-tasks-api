FROM mongo
COPY ["./docker/mongodb/db-settings.sh", "./docker-entrypoint-initdb.d/mongo-init.sh"]
